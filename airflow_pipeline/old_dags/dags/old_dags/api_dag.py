import json

from airflow.decorators import dag, task
import requests
import pendulum
from tasks.api_task import transform_task_v1
from airflow.operators.python import get_current_context
from docker.types import Mount

def make_request():
    response = requests.get('google.com')
    return response

@dag(
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    tags=["example"],
)
def api_scraping_taskflow_api():
    @task
    def get_articles():
        context = get_current_context()
        ti = context["ti"]
        date = context["execution_date"]
        # print(type(date))
        output_file_path = f'data/output.json'
        date_string = date.to_date_string()
        # print(output_file_path)
        print(f"execution date {date_string}")
        with open(f'/data/{date_string}.json', 'w') as f:
            output = {'articles':['ab', 'bc']}
            json.dump(output, f)
        # return {'articles':[1,2,3], 'output': f'/data/{date_string}.json'}
        return {'output_file_path':'output'}
    articles_result = get_articles()
    output_file_path = articles_result['output_file_path']


    @task.docker(image="ingest_repository:airflow",
                 multiple_outputs=True,
                 # xcom_all=True,
                 # environment={'COLLECTION':'ENV-TEST','input-path':output_file_path},
                 python_command="python3 /app/ingest/entry.py",
                 auto_remove="success",
                 # mounts=['/home/Kvothe/data/azath/pipelines/airflow_pipeline:/data'],
                 )

    def transform(articles):
        print("from transform")
        print(f"intput {articles['output_file_path']}")

    transform(articles_result)


    # @task.docker(image="ingest_repository:airflow",
    #              multiple_outputs=True,
    #              xcom_all=True,
    #              environment={'COLLECTION':'ENV-TEST'},
    #              python_command="python3 /app/ingest/entry.py",
    #              auto_remove="success"
    #              )
    # def transform(articles):
    #     """
    #     #### Transform task
    #     A simple Transform task which takes in the collection of order data and
    #     computes the total order value.
    #     """
    #     # articles = articles['articles']
    #     # print(articles)
    # transform(articles_result)

    # articles = get_articles("battlaa")
    # transform()

    # @task.python(python_callable=make_request)
    # def make_request_task():
    #     return {'a':'ab'}
    #
    # @task(multiple_outputs=True)
    # def extract(article_ids_inputs: dict):
    #     """
    #     #### Transform task
    #     A simple Transform task which takes in the collection of order data and
    #     computes the total order value.
    #     """
    #     print(article_ids_inputs)
    #     total = sum(article_ids_inputs['article_ids'])
    #     return {"total_order_value": total}
    # output = transform_task_v1()
    # extract(output)
    # make_request_task()
api_scraping_taskflow_api()
