# from airflow.decorators import task, task_group, dag
# from airflow.models.dag import DAG
# import pendulum
# @task
# def task_start():
#     return "[Task_start]"
#
# @task
# def task_1(value: int) -> str:
#     return f"[Task1 {value}"
#
# @task
# def task_3(value: str) -> None:
#     """Empty Task3"""
#     print(f"[ Task3 {value} ]")
#
#
# @task_group
# def task_group_function(value: int) -> None:
#     task_3(task_1())
#
# @dag(
#     schedule=None,
#     start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
#     catchup=False,
#     tags=["task_group_example"],
# )
# def task_group_example():
#     task_start()
#     task_group_function()
#
# task_group_example()