import json
from airflow.decorators import dag, task
import requests
from datetime import datetime
import pendulum
from airflow.operators.python import get_current_context
from airflow.providers.docker.operators.docker import DockerOperator
from docker.types import Mount

def make_request():
    response = requests.get('google.com')
    return response

# @task
# def t1():
#     pass
#
t2 = DockerOperator(
        task_id="t2",
        image='ingest_repository:airflow',
        command='python3 /app/ingest/entry.py',
        mounts=[Mount(source='airflow-task-volume', target='/data')],
        environment={
            '':''
        }
    )
#
#
# @task.docker(image="ingest_repository:airflow",
#              # multiple_outputs=True,
#              xcom_all=False,
#              # environment={'COLLECTION':'ENV-TEST','input-path':output_file_path},
#              python_command="python3 /app/ingest/entry.py",
#              auto_remove="success",
#              mounts=[Mount(source='airflow-task-volume', target='/data')],
#              )

@dag(
    schedule=None, start_date=pendulum.datetime(2021, 1, 1, tz="UTC"), catchup=False, tags=["example"],
)
def scrape_dag():
    @task
    def t3():
        print("hooray")
    @task
    def t5():
        print("hooray")

    t3() >> t2


scrape_dag()
