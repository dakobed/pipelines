from airflow.decorators import task, dag

@dag()
def best_dag():
    @task.bash
    def also_run_this() -> str:
        return 'echo "ti_key={{ task_instance_key_str }}"'

    also_this = also_run_this()

best_dag()

