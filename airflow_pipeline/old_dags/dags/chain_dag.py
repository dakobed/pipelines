from airflow.decorators import dag, task
from airflow.models.baseoperator import chain, chain_linear
from airflow.operators.bash import BashOperator
import pendulum
# @task
# def task1():
#     pass
#
# @task
# def task2():
#     pass
#
# @task
# def task2():
#     pass
#
# @task
# def task3():
#     pass
#
# @task
# def task4():
#     pass
#
# @task
# def task5():
#     pass
#
# @task
# def task6():
#     pass




@dag(
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    tags=["example"],
)
def chain_dag():
    bash1 = BashOperator(bash_command="echo bash1", task_id="bash1")
    bash2 = BashOperator(bash_command="echo bash2", task_id="bash2")
    bash3 = BashOperator(bash_command="echo bash3", task_id="bash3")
    bash4 = BashOperator(bash_command="echo bash4", task_id="bash4")
    bash5 = BashOperator(bash_command="echo bash5", task_id="bash5")
    bash6 = BashOperator(bash_command="echo bash6", task_id="bash6")

    chain_linear(bash1, bash2,[bash3, bash4], [bash5, bash6])

chain_dag()