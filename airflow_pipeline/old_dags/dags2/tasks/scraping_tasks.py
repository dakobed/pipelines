from airflow.decorators import dag, task


@task(multiple_outputs=True)
def transform_task_v1():
    """
    #### Transform task
    A simple Transform task which takes in the collection of order data and
    computes the total order value.
    """
    return {"article_ids": [301.27, 433.21, 502.22]}