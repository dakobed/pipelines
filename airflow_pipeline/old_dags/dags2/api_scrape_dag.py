import json

import pendulum
import datetime

import requests
from airflow.decorators import dag, task
from docker.types import Mount
@dag(
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    tags=["example"],
)
def api_scrape_pipeline():
    """
    ### TaskFlow API Tutorial Documentation
    This is a simple data pipeline example which demonstrates the use of
    the TaskFlow API using three simple tasks for Extract, Transform, and Load.
    Documentation that goes along with the Airflow TaskFlow API tutorial is
    located
    [here](https://airflow.apache.org/docs/apache-airflow/stable/tutorial_taskflow_api.html)
    """
    @task()
    def extract():
        """
        #### Extract task
        A simple Extract task to get data ready for the rest of the data
        pipeline. In this case, getting data is simulated by reading from a
        hardcoded JSON string.
        """

        article_ids = {'articleids':['a1', 'a2']}
        return article_ids

    @task(multiple_outputs=True)
    def tdd():
        """
        #### Transform task
        A simple Transform task which takes in the collection of order data and
        computes the total order value.
        """

        articles = [
            {
                '_id': 'starbuck'
            },
            {
                '_id': 'ishmael'
            },
            {
                '_id': 'tashtego'
            }
        ]

        second_return_value = [
            {
                '_id': 'starbuck1'
            },
            {
                '_id': 'ishmael1'
            },
            {
                '_id': 'tashtego1'
            }
        ]

        return {'articles': articles, 'second': second_return_value}

    @task.docker(image="ingest_repository:airflow",
                 multiple_outputs=True,
                 # xcom_all=True,
                 environment={'COLLECTION':'ENV-TEST'},
                 python_command="python3 /app/ingest/entry.py --version",

                 auto_remove="success"
                 )
    def transform(articles):
        """
        #### Transform task
        A simple Transform task which takes in the collection of order data and
        computes the total order value.
        """
        print(f"Transform articles {articles}")
        total_order_value = 0
        return {"total_order_value": total_order_value}

    @task()
    def someting(inputs):
        print(f"INPUS {inputs}")
        return {'something': 'va'}
    # @task.docker(image="ingest_repository", multiple_outputs=True, container_name="IngestionContainer", )
    # def container_task():
    #     """
    #     #### Transform task
    #     A simple Transform task which takes in the collection of order data and
    #     computes the total order value.
    #     """
    #     pass
    # #
    # order_data = extract()
    articles = tdd()
    xcoms_result = transform(articles)
    someting(inputs=xcoms_result)


    # result = container_task()

api_scrape_pipeline()
