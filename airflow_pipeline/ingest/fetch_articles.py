import json
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('input_file_path', type=str,
                    help='an integer for the accumulator')

args = parser.parse_args()

with open(f"{args.input_file_path}") as f:
    data = json.load(f)

if __name__ == '__main__':
    print("Fetch Articles script")
    print(data)