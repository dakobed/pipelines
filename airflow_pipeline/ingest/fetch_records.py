import json
import sys

import json
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('date', type=str,
                    help='Time stamp which to make request to the API')

args = parser.parse_args()

if __name__ == '__main__':
    # print(args.date)
    output_file_path = f"/data/{args.date}_records.json"
    output_dictionary = {
        'record_ids': ['record1', 'record2', 'record3']
    }
    with open(output_file_path, 'w') as f:
        json.dump(output_dictionary, f)

    print(output_file_path)



