#!/usr/bin/env python3
import os

import aws_cdk as cdk

from airflow_deployment.airflow_deployment_stack import Ec2IngestStack
from airflow_deployment.amazon_linux_ec2_stack import AmazonLinuxEC2Stack
import yaml
from pathlib import Path

account = os.getenv('AWS_ACCOUNT')
region = "us-west-2"
env = cdk.Environment()
config = yaml.safe_load(Path('config.yaml').read_text())

environment_name = config['environment_name']

if account and region:
    env = cdk.Environment(account=account, region=region)

app = cdk.App()
# Ec2IngestStack(app,
#                stack_name=f"EC2-Airflow-Stack-{environment_name}",
#                construct_id=f"EC2-Airflow-Stack-{environment_name}",
#                config=config,
#                env=env
#     )

AmazonLinuxEC2Stack(app,
               stack_name=f"EC2-Airflow-Stack-{environment_name}",
               construct_id=f"EC2-Airflow-Stack-{environment_name}",
               config=config,
               env=env
    )


app.synth()
