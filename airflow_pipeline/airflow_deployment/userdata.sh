#!/bin/bash

yum -y update
yum -y install ruby
yum -y install wget

cd /home/ec2-user

echo "net.ipv4_ip_forward=1" >> /etc/sysctl.conf

amazon-linux-extras install docker
service docker start
systemctl enable docker
usermod -a -G docker ec2-user

sudo -u ec2-user bash -c 'DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker};mkdir -p $DOCKER_CONFIG/cli-plugins;curl -SL https://github.com/docker/compose/releases/download/v2.27.0/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose;chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose'

