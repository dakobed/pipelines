#!/bin/bash

export AWS_ACCOUNT=$(aws sts get-caller-identity --query Account --output text)
cdk synth --require-approval never
cdk deploy --require-approval never

export PUBLIC_DNS_IP=$(aws cloudformation --region us-west-2 describe-stacks --stack-name Ec2IngestStack-development | jq -r '.Stacks[].Outputs | reduce .[] as $i ({}; .[$i.OutputKey] = $i.OutputValue) | .ingestec2dns')
echo ${PUBLIC_DNS_IP}

#export host="ec2-user@${PUBLIC_DNS_IP}"
#until $(nc -zw 1 ${PUBLIC_DNS_IP} 22)
#do
#    echo -n .''
#    sleep 1
#done
#echo 'Done'
#
