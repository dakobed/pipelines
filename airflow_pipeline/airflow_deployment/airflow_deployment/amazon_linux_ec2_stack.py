from aws_cdk import (
    Stack,
    aws_iam as iam,
    aws_ec2 as ec2,
    CfnOutput,
    aws_ecr as ecr
)
from constructs import Construct


class AmazonLinuxEC2Stack(Stack):
    def _create_role(self, envname):

        managed_cloud_watch_policy = iam.ManagedPolicy.from_managed_policy_arn(self,
                                                                               id=f'cloudwatch_policy_{envname}',
                                                                               managed_policy_arn='arn:aws:iam::aws:policy/CloudWatchLogsFullAccess'
                                                                               )
        managed_s3_policy = iam.ManagedPolicy.from_managed_policy_arn(self,
                                                                      id=f's3_policy_{envname}',
                                                                      managed_policy_arn='arn:aws:iam::aws:policy/AmazonS3FullAccess'
                                                                      )
        role = iam.Role(
            self,
            f"dassem-ec2-{envname}-role",
            role_name=f"dassem-ec2-{envname}-role",
            assumed_by=iam.ServicePrincipal('ec2.amazonaws.com'),
            managed_policies=[managed_cloud_watch_policy, managed_s3_policy]
        )

        inline_policies = {
            'ecr': {
                'actions': [
                    "ecr:GetAuthorizationToken",
                    "ecr:BatchCheckLayerAvailability",
                    "ecr:GetDownloadUrlForLayer",
                    "ecr:GetRepositoryPolicy",
                    "ecr:DescribeRepositories",
                    "ecr:ListImages",
                    "ecr:DescribeImages",
                    "ecr:BatchGetImage",
                    "ecr:GetLifecyclePolicy",
                    "ecr:GetLifecyclePolicyPreview",
                    "ecr:ListTagsForResource",
                    "ecr:DescribeImageScanFindings"
                ],
                'resources': ["*"]
            },
            's3': {
                'actions': [
                    "s3:*"
                ],
                'resources': [
                    "arn:aws:s3:::dassem-records/*"
                ]
            }
        }

        for policy_type, policy in inline_policies.items():
            role.attach_inline_policy(
                iam.Policy(
                    self,
                    f"dassem_{policy_type}_policy_{envname}",
                    policy_name=f"dassem_ecr_{policy_type}_policy_{envname}",
                    statements=[
                        iam.PolicyStatement(
                            actions=policy['actions'],
                            resources=policy['resources']
                        )
                    ]
                )
            )
        return role


    def __init__(self, scope: Construct, construct_id: str, config, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        envname = config.get('environment_name')

        vpc = ec2.Vpc.from_lookup(self,
                                  "dassem_VPC",
                                  vpc_id="vpc-0d705ca3b0611ce09",
                                  is_default=False
                                  )
        security_group = ec2.SecurityGroup(
            self,
            f"dassem_security_group_{envname}",
            security_group_name=f"dassem_security_group_{envname}",
            vpc=vpc,
        )

        security_group.add_ingress_rule(ec2.Peer.any_ipv4(), ec2.Port.tcp(22))
        security_group.add_ingress_rule(ec2.Peer.any_ipv4(), ec2.Port.tcp(8080), description="Webserver Ingress")


        with open('userdata.sh') as f:
            script = f.read()

        user_data = ec2.UserData.custom(script)

        role = self._create_role(envname)
        #
        # ecr.Repository(self,
        #                "Ingest Repository",
        #                repository_name="Ingest Repository"
        #                )

        instance = ec2.Instance(self,

                                id='dassem_ingest_instance',
                                vpc=vpc,
                                security_group=security_group,
                                user_data=user_data,
                                role=role,
                                # instance_type=ec2.InstanceType.of(
                                #     ec2.InstanceClass.BURSTABLE3,
                                #     ec2.InstanceSize.SMALL
                                # ),
                                instance_type=ec2.InstanceType("t2.large"),
                                vpc_subnets=ec2.SubnetSelection(
                                    subnet_type=ec2.SubnetType.PUBLIC
                                ),

                                machine_image=ec2.MachineImage.latest_amazon_linux(
                                    generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX_2),
                                key_name='dassem',
                                )
        CfnOutput(self, 'ingest-ec2-ip', value=instance.instance_private_ip)
        CfnOutput(self, 'ingest-ec2-dns', value=instance.instance_public_dns_name)