import json
import os

from airflow.decorators import dag, task
import requests
from datetime import datetime
import pendulum

from airflow.operators.python import get_current_context
from airflow.providers.docker.operators.docker import DockerOperator
from docker.types import Mount
import boto3
def make_request():
    response = requests.get('google.com')
    return response


@dag(
    schedule=None, start_date=pendulum.datetime(2021, 1, 1, tz="UTC"), catchup=False, tags=["example"],
)
def api_final_dag():
    # @task
    # def get_buckets():
    #     s3 = boto3.client('s3')
    #     print(s3.list_buckets())
    # get_buckets()
    @task
    def get_date(ti):
        context = get_current_context()
        ti = context["ti"]
        date = context["execution_date"]
        date_string = date.to_date_string()
        return date_string
    #
    # fetch_records = DockerOperator(
    #     task_id='fetch_records',
    #     image='ingest_repository:airflow',
    #     api_version='auto',
    #     command="python3 /app/ingest/fetch_records.py {{ ti.xcom_pull(task_ids='get_date') }}",
    #     do_xcom_push=True,
    #     xcom_all=False,
    #     network_mode='bridge',
    #     mounts=[Mount(source='airflow-task-volume', target='/data')],
    # )
    #
    # fetch_articles = DockerOperator(
    #     task_id='fetch_articles',
    #     image='ingest_repository:airflow',
    #     api_version='auto',
    #     command="python3 /app/ingest/fetch_articles.py {{ ti.xcom_pull(task_ids='fetch_records') }}",
    #     do_xcom_push=True,
    #     xcom_all=False,
    #     network_mode='bridge',
    #     mounts=[Mount(source='airflow-task-volume', target='/data')]
    # )
    #
    load_articles = DockerOperator(
        task_id='load_articles',
        image='ingest_repository:airflow',
        api_version='auto',
        command="python3 /app/ingest/load_articles.py ",
        do_xcom_push=True,
        xcom_all=False,
        network_mode='bridge',
        mounts=[Mount(source='airflow-task-volume', target='/data')],
        environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "REGION_NAME": os.getenv('REGION_NAME')
        }
    )
    #
    #
    #
    get_date() >> load_articles # fetch_records >> fetch_articles >> load_articles

api_final_dag()










#
# load_task = DockerOperator(
#     task_id="load_data",
#     image='ingest_repository:airflow',
#     command='python3 /app/ingest/entry.py',
#     mounts=[Mount(source='airflow-task-volume', target='/data')],
#     environment={
#         'mode': 'load'
#     }
# )
#
# record_ids_task = DockerOperator(
#     task_id="fetch_record_ids",
#     image='ingest_repository:airflow',
#     command='python3 /app/ingest/entry.py',
#     mounts=[Mount(source='airflow-task-volume', target='/data')],
#     environment={
#         'mode': 'records'
#     }
# )
#
# record_ids_task = DockerOperator(
#     task_id="fetch_record_ids",
#     image='ingest_repository:airflow',
#     command='python3 /app/ingest/entry.py',
#     mounts=[Mount(source='airflow-task-volume', target='/data')],
#     environment={
#         'mode': 'records'
#     }
# )


# fetch_articles_task = DockerOperator(
#     task_id="fetch_articles",
#     image='ingest_repository:airflow',
#     command='python3 /app/ingest/entry.py',
#     mounts=[Mount(source='airflow-task-volume', target='/data')],
#     environment={
#         'mode': 'articles'
#     }
# )
#
# write_xcom_docker_pull = DockerOperator(
#     task_id='write_xcom_docker_pull',
#     image='ubuntu:20.04',
#     api_version='auto',
#     command="""echo {{ ti.xcom_pull(task_ids="write_xcom_docker_warning") }}""",
#     do_xcom_push=True,
#     xcom_all=False,
#     network_mode='bridge')
#
# echo_xcom = DockerOperator(
#     task_id='echo_xcom',
#     image='ingest_repository:airflow',
#     api_version='auto',
#     command="echo {{ ti.xcom_pull(task_ids='fetch_articles') }}",
#     do_xcom_push=True,
#     xcom_all=False,
#     network_mode='bridge'
# )

# fetch_articles_task >> echo_xcom
# record_ids_task >> fetch_articles_task >> load_task >> open_task >> echo_xcom
