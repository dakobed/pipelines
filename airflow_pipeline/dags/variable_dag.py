from airflow.models import Variable
from airflow.decorators import task, dag
import pendulum


foo = Variable.get("foo")

# # Auto-deserializes a JSON value
# bar = Variable.get("bar", deserialize_json=True)
#
# # Returns the value of default_var (None) if the variable is not set
# baz = Variable.get("baz", default_var=None)


@dag(
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    tags=["example"],
)
def best_dag():
    @task.bash
    def also_run_this() -> str:
        print(f"The foo variable is {foo}")
        return 'echo "ti_key={{ task_instance_key_str }}"'

    also_this = also_run_this()

best_dag()

